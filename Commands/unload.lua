function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "listOfUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "target",
				variableType = "table",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local originalPosition = {}
local previousPosition = {}
local currentPosition = {}

local function ClearState(self)
	self.running=false
end



function Run(self, units, parameter)
	self.threshold = 1500


	local listOfUnits = parameter.listOfUnits -- table
	local targetPosition = parameter.target

	local target = targetPosition:AsSpringVector()


	if not self.running then
		-- for i=1, #listOfUnits do
		-- 	local x, y, z = Spring.GetUnitPosition(listOfUnits[i])
		-- 	originalPosition[i] = Vec3(x, y, z)
		-- 	previousPosition[i] = Vec3(0, 0, 0)
		-- end
		for i = 1, #listOfUnits  do
			-- local source = listOfsources[i]:AsSpringVector()
			-- local origin = originalPosition[i]:AsSpringVector()


			SpringGiveOrderToUnit(listOfUnits[i], CMD.UNLOAD_UNITS, {target[1] + math.random(500) - 250, target[2], target[3] + math.random(500) - 250, 100}, {"shift"})
			SpringGiveOrderToUnit(listOfUnits[i], CMD.MOVE, {3000 + i * 500, 0, 100}, {"shift"})
		end
		self.running = true
	end
	



	for i=1, #listOfUnits do
		local health, maxHealth, q1, q2, q3 = Spring.GetUnitHealth(listOfUnits[i])
		if health ~= maxHealth then
			return RUNNING
		end
	end
	return SUCCESS
	
end


function Reset(self)
	ClearState(self)
end
